from typing import Any, Awaitable, Callable, Dict

from aiogram import BaseMiddleware
from aiogram.dispatcher.flags import get_flag
from aiogram.filters import CommandObject
from aiogram.types import Message
from aiogram.utils.i18n import I18n
from stfu_tg import Section, Doc, Underline, Url, Strikethrough

from ass_tg.entities import ArgEntities
from ass_tg.exceptions import ARGS_EXCEPTIONS, ArgError
from ass_tg.i18n import gettext as _
from ass_tg.i18n import gettext_ctx
from ass_tg.types.tuple import TupleArg


class ArgsMiddleware(BaseMiddleware):

    def __init__(
            self,
            error_additional_items=(Url('Syntax help', 'https://github.com/'),)
    ):
        super().__init__()
        self.error_additional_items = error_additional_items

    @staticmethod
    def highlight_failed_argument(error: ArgError, command: CommandObject) -> tuple[str, ...]:
        # TODO: use variable names for magic numbers
        raw_text = command.args
        error_offset = error.offset
        error_length = error.length

        if not raw_text:
            return '',

        command_prefix = '!' if command.prefix == '/' else command.prefix
        text = f'{command_prefix}{command.command} '

        # Get prefix
        prefix = raw_text[:error_offset]
        if len(prefix) > 15:
            prefix = f'... {prefix[-15:]}'
        text += prefix

        # Get error text
        # TODO: what if its too long?
        if error_length:
            if error.strikethrough:
                text += str(Strikethrough(raw_text[error_offset:error_offset + error_length]))
            else:
                text += str(Underline(raw_text[error_offset:error_offset + error_length]))

            # Get postfix
            if len(postfix := raw_text[error_offset + error_length:]) > 15:
                postfix = f'{postfix[:15]} ...'
            text += postfix

        else:
            if len(error_text := raw_text[error_offset:]) > 25:
                error_text = f'{error_text[:25]} ...'
            text += str(Underline(error_text))

        return f'<i>{text}</i>',

    async def send_error(self, message: Message, error: ArgError, command: CommandObject) -> None:
        doc = Doc()
        doc += Section(
            *(self.highlight_failed_argument(error, command)) if hasattr(error, 'offset') else tuple(),
            ' ' if hasattr(error, 'offset') and error.offset else '',
            *error.doc,
            *self.error_additional_items,
            title=_('[Argument validation error]'),
            title_underline=False,
        )

        await message.reply(str(doc), disable_web_page_preview=True)

    async def __call__(
        self,
        handler: Callable[[Message, Dict[str, Any]], Awaitable[Any]],
        message: Message,
        data: Dict[str, Any]
    ) -> Any:

        if base_arg := get_flag(data, "args"):
            if isinstance(base_arg, dict):
                base_arg = TupleArg(**base_arg)
            elif not isinstance(base_arg, TupleArg):
                raise ValueError

            command: CommandObject = data['command']

            if text := command.args:
                i18n = data.get('i18n', I18n(path='/'))
                with i18n.context():
                    gettext_ctx.set(i18n)
                    try:
                        arg = base_arg(text, 0, ArgEntities(message.entities or [], command))
                        data['args'] = arg.value  # Old ASS compatibility
                        data['arg'] = arg
                    except ARGS_EXCEPTIONS as e:
                        await self.send_error(message, e, command)
                        return

        return await handler(message, data)
