from typing import Any, Generic, List, Tuple, TypeVar

from babel.support import LazyProxy
from stfu_tg import Code, Italic

from ass_tg.entities import ArgEntities
from ass_tg.exceptions import (ArgCustomError, ArgInListItemError, ArgListEmpty, ArgTypeError, ArgIsRequiredError)
from ass_tg.i18n import gettext as _
from ass_tg.types.wrapped import WrappedArgFabricABC

ListArgItemType = TypeVar('ListArgItemType')


class ListArg(WrappedArgFabricABC[List[Any]], Generic[ListArgItemType]):
    # TODO: Make folded lists work
    list_separator: str
    list_start: str
    list_end: str

    def __init__(self, *args, separator=',', list_start='(', list_end=')'):
        super().__init__(*args)

        self.list_separator = separator
        self.list_start = list_start
        self.list_end = list_end

    @property
    def needed_type(self) -> Tuple[LazyProxy, LazyProxy]:
        # TODO: Show settings??
        return (
            # Here we use plural, because lists contains many items
            LazyProxy(lambda: _("List of {}").format(self.child_fabric.needed_type[1])),
            LazyProxy(lambda: _("Lists of {}").format(self.child_fabric.needed_type[1]))
        )

    def check(self, text: str, entities: ArgEntities) -> bool:
        if not (text.startswith(self.list_start) and self.list_end in text):
            return False

        if text.find(self.list_start) + 1 == text.find(self.list_end):
            raise ArgListEmpty(needed_type=self.needed_type, description=self.description)
        return True

    def parse(
            self,
            text: str,
            offset: int,
            entities: ArgEntities
    ) -> Tuple[int, List[ListArgItemType]]:
        text = text.removeprefix(self.list_start).replace(self.list_end, '')

        items = []

        # Add syntax symbols to length initially
        length = len(self.list_start)

        while text:
            separator_index = text.find(self.list_separator)
            has_separator = separator_index != -1

            arg_text = text if self.child_fabric.know_the_end or not has_separator else text[:separator_index]

            if not arg_text.strip():
                raise ArgIsRequiredError(self.child_fabric.description)

            arg_offset = offset + length
            arg_entities = entities.get_arg(arg_offset, len(arg_text))

            try:
                arg = self.child_fabric(
                    arg_text,
                    arg_offset,
                    arg_entities,
                    known_end_arg_text=text,
                    not_known_end_arg_text=text[:separator_index] if has_separator else text
                )

            except ArgTypeError as e:
                raise ArgInListItemError(e) from e

            items.append(arg)

            if not self.child_fabric.know_the_end:
                text = text[separator_index + 1:] if has_separator else ''
                stripped_text = text.lstrip()
                length += len(text) - len(stripped_text)

                # Calculate the number of extra symbols that wasn't occupied by the argument
                if has_separator:
                    length += len(arg_text[arg.length:])
                    length += len(self.list_separator)
            else:
                text = text[arg.length:]

                next_separator = text.find(self.list_separator)
                if next_separator != -1 and not text.startswith(self.list_separator):
                    if next_seperator != -1:
                        length = next_seperator - length -1
                    else:
                        length = len(arg_text)

                    raise ArgCustomError(
                        LazyProxy(lambda: _(
                            "Argument '{arg_text}' was parsed, but it has unknown text after it!",
                            "Please ensure you correctly divided all of arguments."
                        ).format(arg_text = Italic(arg_text[:arg.length]))),
                        offset = length + arg.length,
                        length = length,
                        strikethrough = True
                    )

                length += arg.length
                stripped_text = text.lstrip()
                length += len(text) - len(stripped_text)
                stripped_text_len = len(stripped_text)
                stripped_text = stripped_text.removeprefix(self.list_seperator).lstrip()
                length += stripped_text_len - len(stripped_text)

            if not has_separator:
                # No more separators - returning
                break

            if not arg.length and not text:
                # Argument has no length, and no text left - Means this argument consumes all text
                break

        # Add the end
        length += len(self.list_end)

        return length, items


class DividedArg(ListArg):

    def __init__(self, *args, separator='|'):
        super().__init__(*args, separator=separator, list_start='', list_end='')
