from typing import Tuple, Dict

from ass_tg.entities import ArgEntities
from ass_tg.exceptions import ArgIsRequiredError
from ass_tg.types.base_abc import ParsedArg, ArgFabric


class ParsedArgs(Dict[str, ParsedArg]):
    pass


class TupleArg(ArgFabric):
    """
    Represents a basic and the first argument, which contains the child ones.
    Each argument contains its name and a fabric.
    Implements arguments validation.
    """

    fabrics: Dict[str, ArgFabric]

    def __init__(self, *args, **kwargs):
        super().__init__(*args)

        self.fabrics = kwargs

    @property
    def needed_type(self):
        return "", ""

    @staticmethod
    def check(text: str, entities: ArgEntities) -> bool:
        # Contains no check as it as there's no specific argument type.
        # The validation will be done in the parse() function
        return True

    def parse(
            self,
            text: str,
            offset: int,
            entities: ArgEntities
    ) -> Tuple[int, ParsedArgs]:

        args_data = ParsedArgs()
        length = 0

        for arg_codename, arg_fabric in self.fabrics.items():
            # Strip text and count offset
            stripped_text = text.lstrip()
            stripped_offset = len(stripped_text)
            offset += len(text) - stripped_offset

            arg_entities = entities.get_arg(offset, stripped_offset)

            if not text.strip():
                raise ArgIsRequiredError(arg_fabric.description)

            arg = arg_fabric(stripped_text, offset, arg_entities)
            length += arg.length
            args_data[arg_codename] = arg

            offset += arg.length
            text = text[arg.length + stripped_offset]

            if not arg.length and stripped_text:
                # Argument has no length, and no text left - Means this argument consumes all text
                break

        return length, args_data
