import pytest

from ass_tg.types.long import RestTextArg
from ass_tg.types.oneword import WordArg
from ass_tg.types.stacked import StackedArg
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,cur_arg,stack_arg', [
    ('Foo(Bar)', 'Foo', 'Bar'),
    ('Foo  (Bar)', 'Foo', 'Bar'),
    ('Fo (Bar  )', 'Fo', 'Bar  '),
    ('Fo  (  Bar   )', 'Fo', 'Bar   '),
    ('Foo(Bar) extra text', 'Foo', 'Bar'),
])
def test_stacked_rest_text_in_word_arg(text: str, cur_arg: str, stack_arg: str):
    arg = StackedArg(
        current_arg=WordArg(),
        stacked_arg=RestTextArg()
    )(text, 5, NO_ENTITIES)

    assert arg.value[0] == cur_arg
    assert arg.value[1] == stack_arg
    assert arg.length == len(text)


@pytest.mark.parametrize('text,cur_arg,stack_arg', [
    ('Foo(Bar)', 'Foo', 'Bar'),
    ('Foo  (Bar)', 'Foo', 'Bar'),
    ('Fo (Bar  )', 'Fo', 'Bar'),
    ('Fo  (  Bar   )', 'Fo', 'Bar'),
    ('Foo(Bar) extra text', 'Foo', 'Bar'),
])
def test_stacked_word_arg_in_word_arg(text: str, cur_arg: str, stack_arg: str):
    arg = StackedArg(
        current_arg=WordArg(),
        stacked_arg=WordArg()
    )(text, 5, NO_ENTITIES)

    assert arg.value[0] == cur_arg
    assert arg.value[1] == stack_arg
    assert arg.length == len(text)


@pytest.mark.parametrize('text,cur_arg,stack_arg', [
    ("Foo(Bar(Dar))", 'Foo', ('Bar', 'Dar'))
])
def test_stacked_in_stacked_rest_text_in_word_arg(text: str, cur_arg: str, stack_arg: str):
    arg = StackedArg(
        current_arg=WordArg(),
        stacked_arg=StackedArg(
            current_arg=WordArg(),
            stacked_arg=RestTextArg()
        )
    )(text, 5, NO_ENTITIES)

    assert arg.value[0] == cur_arg
    assert arg.value[1] == stack_arg
    assert arg.length == len(text)
