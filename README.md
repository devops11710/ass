# Arguments Stupendous Searcher (ASS)

The library used to parse arguments from the message
Works only with aiogram3

See also:
* [Sophie Text Formatting Utility library (STFU)](https://gitlab.com/SophieBot/stf)

## Quick example:
Let's create a handler for a tban (temporary ban) command using ASS arguments.
```python
from ass_tg.types import WordArg, ActionTimeArg, RestTextArg, OptionalArg
from ass_tg.types.base_abc import ParsedArg

from stfu_tg import Section, KeyValue


@dp.message(Command('tban'))  # Set a command filter
@flags.args(
        user=WordArg("User"),
        time=ActionTimeArg("Time to ban"),
        description=RestTextArg("Reason text")
)
async def tban_user_handler(msg: Message, args: Dict[str, ParsedArg]):
    user: ParsedArg[str] = args['user']
    time: ParsedArg[timedelta] = args['time']
    description: ParsedArg[str] = args['description']

    # Here we used STFU's formatting, but you can use variables as you wish!
    await msg.reply(str(Section(
        KeyValue("User", user.value),
        KeyValue("On", time.value or 'Always'),
        KeyValue("Description", description.value or "No description"),
        title="Ban"
    )))

```

The argument parser contains inbuilt type and argument checker:

![img.png](images/img_1.png)

The library contains a lot of unique types, such as:
* Action time
* WordArg
* IntArg
* BooleanArg
* RestTextArg
* QuotedTextArg

It also contains a logic arguments types:
* OptionalArg
* OrArg
* ListArg and DividedArg

And allows to make your own types!