from typing import Optional

import pytest

from ass_tg.types.logic import OptionalArg
from ass_tg.types.long import RestTextArg
from ass_tg.types.oneword import WordArg
from ass_tg.types.tuple import TupleArg
from tests.data import NO_ENTITIES


def test_tuple_arg():
    arg = TupleArg(
        one=WordArg(),
        second=RestTextArg()
    )('Example foo bar', 5, NO_ENTITIES)
    assert arg.value['one'].value == 'Example'
    assert arg.value['second'].value == 'foo bar'
