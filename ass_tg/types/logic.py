import contextlib
from typing import Any, Optional, Tuple

from babel.support import LazyProxy

from ass_tg.entities import ArgEntities
from ass_tg.exceptions import ArgTypeError
from ass_tg.i18n import lazy_gettext as l_
from ass_tg.types.base_abc import ArgFabric
from ass_tg.types.wrapped import WrappedArgFabricABC


class OptionalArg(WrappedArgFabricABC):

    @property
    def needed_type(self) -> Tuple[LazyProxy, LazyProxy]:
        return (
            l_("Optional {}").format(self.child_fabric.needed_type[0]),
            l_("Optionals {}").format(self.child_fabric.needed_type[0]),
        )

    def check(self, text: str, entities: ArgEntities) -> bool:
        return True

    def parse(self, text: str, offset: int, entities: ArgEntities) -> Tuple[Optional[int], Any]:
        if self.child_fabric.check(text, entities):
            with contextlib.suppress(ArgTypeError):
                arg = self.child_fabric(text, offset, entities)
                return arg.length, arg.value
        return 0, None


class OrArg(ArgFabric):
    args_type: Tuple[ArgFabric, ...]

    def __init__(self, *args_type: ArgFabric):
        super().__init__(args_type[0].description)
        self.args_type = args_type

    @property
    def needed_type(self) -> Tuple[LazyProxy, LazyProxy]:
        return (l_(" or ").join(f"'{arg.needed_type[0]}'" for arg in self.args_type),
                l_(" or ").join(f"'{arg.needed_type[1]}'" for arg in self.args_type))

    @staticmethod
    def check(text: str, entities: ArgEntities) -> bool:
        return bool(text)

    def pre_parse(
            self,
            raw_text: str,
            offset: int,
            entities: ArgEntities,
            **data
    ) -> Tuple[int, Any]:
        for arg_fabric in self.args_type:
            try:
                if arg_fabric.know_the_end:
                    text = data.get("know_end_arg_text", raw_text)
                else:
                    text = data.get("not_known_end_arg_text", raw_text)

                if not arg_fabric.check(text, entities):
                    continue

                self.know_the_end = arg_fabric.know_the_end
                arg = arg_fabric(text, offset, entities)

                return arg.length, arg.value

            except ArgTypeError:
                continue

        raise ArgTypeError(
            needed_type=self.needed_type,
            description=self.description,
            length=len(raw_text),
            offset=offset
        )

    def __repr__(self):
        return f'<{self.__class__.__name__}>: {", ".join(str(x) for x in self.args_type)}'
