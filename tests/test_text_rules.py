import pytest

from ass_tg.types.long import RestTextArg
from ass_tg.types.oneword import WordArg
from ass_tg.types.text_rules import (UntilArg, StartsWithArg)
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,starts_with,result', [
    ('-> Test', '->', 'Test'),
    ('123Test', '123', 'Test'),
])
def test_int_arg(text: str, starts_with: str, result: str):
    arg = StartsWithArg(starts_with, WordArg())(text, 5, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text,until,chars_before_until,result', [
    ('Foo Bar -> Foo', '->', 1, 'Foo Bar '),
    ('Foo Bar | Foo', '|', 1, 'Foo Bar '),
    ('123 321 231 -> 21311 1234', '->', 1, '123 321 231 '),
    ('Foo Bar', '->', 1, 'Foo Bar'),
])
def test_until_arg(text: str, until: str, chars_before_until: int, result: str):
    arg = UntilArg(until, RestTextArg())(text, 5, NO_ENTITIES)

    assert arg.value == result
    # assert arg.length == len(text)
