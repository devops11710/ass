from dataclasses import dataclass
from typing import Iterable

from aiogram.filters import CommandObject
from aiogram.types import MessageEntity


@dataclass(eq=False, slots=True)
class ArgEntities:
    """Adequate implementation of the message entity"""
    entities: Iterable[MessageEntity]
    command: CommandObject

    def get_arg(self, offset: int, length: int) -> "ArgEntities":
        # TODO: explain the logic in a docstring
        # and use variable names instead of magic values?

        offset += len(self.command.prefix) + len(self.command.command)

        entities = [
            x for x in self.entities
            if x.offset * 2 >= offset and x.length * 2 <= length
        ]

        return ArgEntities(entities, self.command)

    def __bool__(self) -> bool:
        return bool(self.entities)
