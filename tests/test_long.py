
import pytest

from ass_tg.exceptions import ArgTypeError
from ass_tg.types.long import QuotedTextArg, RestTextArg
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,result', [
    ('"Hello world"', "Hello world"),
    ('"Hello world" blahblah', "Hello world"),
    ('"Hello world"" blahblah', "Hello world"),
])
def test_quoted_text_arg(text: str, result: str):
    arg = QuotedTextArg()(text, 5, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == len(result) + 2  # Because of the length of the quotes


@pytest.mark.parametrize('text', [
    '"Hello world',
    'Hello world',
    'Hello world"',
    'Hello "world',
    'Hello "world"',
])
def test_quoted_text_arg_fail(text: str):
    arg = QuotedTextArg()
    assert not arg.check(text, NO_ENTITIES)


@pytest.mark.parametrize('text', [
    '"Hello world',
    'Hello world',
    'Hello world"',
    'Hello "world',
    'Hello "world"',
])
def test_rest_text_arg(text: str):
    arg = RestTextArg()(text, 3, NO_ENTITIES)

    assert arg.value == text
    assert arg.length == len(text)
