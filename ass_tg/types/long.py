from typing import Tuple

from babel.support import LazyProxy

from ass_tg.entities import ArgEntities
from ass_tg.i18n import lazy_gettext as l_
from ass_tg.types.base_abc import ArgFabric


class QuotedTextArg(ArgFabric):
    know_the_end = True

    @property
    def needed_type(self) -> Tuple[LazyProxy, LazyProxy]:
        return l_("Text (in double quotes)"), l_("Texts (in double quotes)")

    def check(self, text: str, entities: ArgEntities) -> bool:
        return bool(text.startswith('"') and text.find('"', 1) != -1)

    def parse(self, text: str, offset: int, entities: ArgEntities) -> Tuple[int, str]:
        # TODO: use variable names instead of magic numbers
        argument, _ = text.removeprefix('"').split('"', 1)

        return len(argument) + 2, argument


class RestTextArg(ArgFabric):

    @property
    def needed_type(self) -> Tuple[LazyProxy, LazyProxy]:
        return l_("Text"), l_("Texts")

    @staticmethod
    def check(text: str, entities: ArgEntities) -> bool:
        return text != ""

    def parse(self, text: str, offset: int, entities: ArgEntities) -> Tuple[int, str]:
        return len(text), text
