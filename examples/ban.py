from datetime import timedelta
from typing import Dict

from aiogram import Bot, Dispatcher, flags
from aiogram.filters import Command
from aiogram.types import Message
from stfu_tg import Section, KeyValue

from ass_tg.middleware import ArgsMiddleware
from ass_tg.types import WordArg, ActionTimeArg, RestTextArg, OptionalArg
from ass_tg.types.base_abc import ParsedArg

bot = Bot(token="TOKEN", parse_mode="html")
dp = Dispatcher()

# Ensure to add arguments parser middleware!
dp.message.middleware(ArgsMiddleware())


@dp.message(Command('ban'))  # Set a command filter
@flags.args(
        user=WordArg("User"),
        time=OptionalArg(ActionTimeArg("Time to ban")),
        description=OptionalArg(RestTextArg("Reason text"))
)
async def ban_user_handler(msg: Message, args: Dict[str, ParsedArg]):
    user: ParsedArg[str] = args['user']
    time: ParsedArg[timedelta] = args['time']
    description: ParsedArg[str] = args['description']

    # Here we used STFU's formatting, but you can use variables as you wish!
    await msg.reply(str(Section(
        KeyValue("User", user.value),
        KeyValue("On", time.value),
        KeyValue("Description", description.value),
        title="Ban"
    )))

