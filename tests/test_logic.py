from typing import Optional

import pytest

from ass_tg.types.logic import OptionalArg
from ass_tg.types.oneword import WordArg
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,result', [
    ('Example text', 'Example'),
    ('', None)
])
def test_optional_word_arg(text: str, result: Optional[str]):
    arg = OptionalArg(WordArg())(text, 5, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == (len(result) if result else 0)
