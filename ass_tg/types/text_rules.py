from typing import Any, Tuple

from ass_tg.entities import ArgEntities
from ass_tg.types.wrapped import WrappedArgFabricABC


class StartsWithArg(WrappedArgFabricABC):
    starts_with: str

    def __init__(self, starts_with: str, *args):
        super().__init__(*args)

        self.starts_with = starts_with

    def check(self, text: str, entities: ArgEntities) -> bool:
        return self.starts_with in text

    def parse(self, raw_text: str, offset: int, entities: ArgEntities) -> Tuple[int, Any]:
        start_text, text = raw_text.split(self.starts_with, 1)

        # Strip text and count offset
        arg_stripped_offset = (len(text) - len(text := text.lstrip())) + len(self.starts_with)

        arg_offset = offset + len(start_text) + arg_stripped_offset
        length, data = super().parse(text, arg_offset, entities.get_arg(arg_offset, len(text)))
        return length + arg_stripped_offset, data


class UntilArg(WrappedArgFabricABC):
    until_text: str

    # TODO: Description

    def __init__(self, until_text: str, *args):
        super().__init__(*args)

        self.until_text = until_text

    def parse(self, text: str, offset: int, entities: ArgEntities) -> Tuple[int, Any]:
        arg_text = text
        if self.until_text in text:
            arg_text, _rest = text.split(self.until_text, 1)

        length, data = super().parse(arg_text, offset, entities.get_arg(offset, len(arg_text)))

        if not length:
            length = len(text)

        return length, data
