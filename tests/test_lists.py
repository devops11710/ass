from typing import Tuple

import pytest

from ass_tg.exceptions import (ArgCustomError, ArgInListItemError, ArgIsRequiredError, ArgListEmpty)
from ass_tg.types.lists import DividedArg, ListArg
from ass_tg.types.logic import OrArg
from ass_tg.types.long import QuotedTextArg
from ass_tg.types.oneword import IntArg, WordArg
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,result', [
    ('(FooBar, TestWord, AnotherWord)', ('FooBar', 'TestWord', 'AnotherWord')),
    ('(FooBar,     TestWord   ,    AnotherWord    ,LastWord)', ('FooBar', 'TestWord', 'AnotherWord', 'LastWord')),
    ('(FooBar)', ('FooBar',)),
    ('(Foo,Bar,Bar)', ('Foo', 'Bar', 'Bar'))
])
def test_list_arg_with_word_arg(text: str, result: Tuple[str]):
    arg = ListArg(WordArg())(text, 5, NO_ENTITIES)

    assert arg.values == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text,result,options', [
    ('[FooBar, TestWord]', ('FooBar', 'TestWord'), {'list_start': '[', 'list_end': ']'}),
    ('(FooBar | TestWord | Foo)', ('FooBar', 'TestWord', 'Foo'), {'separator': '|'}),
    ('FooBar | TestWord | Foo', ('FooBar', 'TestWord', 'Foo'), {'separator': '|', 'list_start': '', 'list_end': ''}),
])
def test_list_custom_syntax(text: str, result: Tuple[str], options: dict):
    arg = ListArg(WordArg(), **options)(text, 5, NO_ENTITIES)

    assert arg.values == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text', [
    '| ',
    '|  | ||',
    ' | | '
])
def test_list_no_syntax_exception(text: str):
    with pytest.raises(ArgIsRequiredError):
        ListArg(WordArg(), separator='|', list_start='', list_end='')(text, 5, NO_ENTITIES)


def test_list_arg_empty():
    with pytest.raises(ArgListEmpty):
        ListArg(WordArg())("()", 5, NO_ENTITIES)


def test_list_child_item_exception():
    with pytest.raises(ArgInListItemError):
        ListArg(IntArg())("(foo)", 5, NO_ENTITIES)


@pytest.mark.parametrize('text,result', [
    ('Foo | Bar | Dar', ('Foo', 'Bar', 'Dar')),
    ('Foo |Bar| Dar', ('Foo', 'Bar', 'Dar')),
    ('Foo|    Bar|   Dar', ('Foo', 'Bar', 'Dar')),
])
def test_divided_arg(text: str, result: Tuple[str]):
    arg = DividedArg(WordArg())(text, 5, NO_ENTITIES)

    assert arg.values == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text,result', [
    ('"Foo" | "Bar" | "|Dar|"', ('Foo', 'Bar', '|Dar|')),
    ('"Foo"     |    "Bar"  |   "|Dar|"', ('Foo', 'Bar', '|Dar|')),
    ('"Foo"     |    "Bar"  |   "  |  Dar | "', ('Foo', 'Bar', '  |  Dar | ')),
])
def test_know_the_end_child(text: str, result: Tuple[str]):
    arg = DividedArg(QuotedTextArg())(text, 5, NO_ENTITIES)

    assert arg.values == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text,result', [
    ('"Foo" | "Bar"', ('Foo', 'Bar')),
    ('Foo| Bar', ('Foo', 'Bar')),
    ('Foo|Bar', ('Foo', 'Bar')),
    ('Foo    |Bar', ('Foo', 'Bar')),
    ('Foo|    Bar', ('Foo', 'Bar')),
    ('Foo    |    Bar', ('Foo', 'Bar')),
    ('Foo| "Bar"', ('Foo', 'Bar')),
    ('"Foo" | Bar', ('Foo', 'Bar')),
    ('"Foo" | "Bar', ('Foo', '"Bar')),
    ('Foo" | "Bar"', ('Foo"', 'Bar')),
])
def test_list_of_ores(text: str, result: Tuple[str]):
    arg = DividedArg(OrArg(QuotedTextArg(), WordArg()))(text, 5, NO_ENTITIES)

    assert arg.values == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text,result', [
    ('"Foo"foobar | "Bar"', ('Foo', 'Bar'))
])
def test_list_known_end_but_item_next(text: str, result: Tuple[str]):
    with pytest.raises(ArgCustomError):
        DividedArg(QuotedTextArg())(text, 5, NO_ENTITIES)
