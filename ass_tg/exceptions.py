from typing import Iterable, Optional, Tuple, Dict

from babel.support import LazyProxy
from stfu_tg import Italic, Section, KeyValue, Code

from ass_tg.i18n import gettext as _


class ArgError(Exception):
    length: Optional[int]
    offset: Optional[int]
    strikethrough: bool = False

    @property
    def doc(self) -> Tuple[str]:
        raise NotImplementedError


class ArgTypeError(ArgError):
    needed_type: Tuple[str | LazyProxy, str | LazyProxy]
    description: Optional[str | LazyProxy]

    def __init__(
            self,
            needed_type: Tuple[str | LazyProxy, str | LazyProxy],
            description: Optional[str | LazyProxy],
            length: Optional[int],
            offset: int,
            examples: Optional[Dict[str, LazyProxy]] = None
    ):
        self.needed_type = needed_type
        self.description = description

        self.length = length
        self.offset = offset

        self.examples = examples or {}

    @property
    def doc(self):
        return (
            _("The highlighted argument ({description}) has an invalid type!").format(
                description=self.description or _("No description")
            ),
            Section(Italic(self.needed_type[0]), title=_("Needed type"), title_bold=False),
            Section(*(KeyValue(Code(k), v, title_bold=False) for k, v in self.examples.items()),
                    title=_("Examples"), title_bold=False) if self.examples else None
        )


class ArgListEmpty(ArgError):
    needed_type: Tuple[str | LazyProxy, str | LazyProxy]
    description: Optional[str | LazyProxy]

    def __init__(self, needed_type: Tuple[str | LazyProxy, str | LazyProxy], description: Optional[str | LazyProxy]):
        self.needed_type = needed_type
        self.description = description

    @property
    def doc(self):
        return (
            _("The list argument ({description}) is empty!").format(description=self.description),
            Section(Italic(self.needed_type[0]), title=_("Needed type"), title_bold=False)
        )


class ArgInListItemError(ArgError):
    child_error: ArgError

    this_arg_length: Optional[int] = None
    this_arg_offset: Optional[int] = None

    def __init__(
            self,
            child_error: ArgError,
            length: Optional[int] = None,
            offset: Optional[int] = None,
    ):
        self.child_error = child_error
        this_arg_length = length
        this_arg_offset = offset

    @property
    def doc(self):
        return self.child_error.doc

    @property
    def length(self):
        return self.this_arg_length or self.child_error.length

    @property
    def offset(self):
        return self.this_arg_offset or self.child_error.offset


class ArgIsRequiredError(ArgError):

    def __init__(self, description: Optional[str | LazyProxy]):
        self.description = description

    @property
    def doc(self):
        return _("Argument ({description}) is required!").format(description=self.description),


class ArgCustomError(ArgError):

    def __init__(
            self,
            *texts: Iterable[str | LazyProxy],
            length: Optional[int] = None,
            offset: Optional[int] = None,
            strikethrough: bool = False
    ):
        self.texts = texts

        self.length = length
        self.offset = offset
        self.strikethrough = strikethrough

    @property
    def doc(self) -> Tuple[str]:
        return *(str(x) for x in self.texts),


ARGS_EXCEPTIONS = (ArgTypeError, ArgIsRequiredError, ArgListEmpty, ArgInListItemError, ArgCustomError)
