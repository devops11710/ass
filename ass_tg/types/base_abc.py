from abc import ABC
from typing import Any, Generic, Optional, Tuple, TypeVar, Dict

from babel.support import LazyProxy

from ass_tg.entities import ArgEntities
from ass_tg.exceptions import ArgTypeError

ArgValueType = TypeVar('ArgValueType')


class ArgFabric(Generic[ArgValueType]):
    """Provides a way to create an argument fabric."""
    description: Optional[LazyProxy | str]

    # Some parameters used by the other arguments
    know_the_end: bool = False
    default_no_value_value: Optional[Any] = None

    def __init__(self, description: Optional[LazyProxy | str] = None):
        self.description = description

    @property
    def needed_type(self) -> Tuple[LazyProxy, LazyProxy]:
        raise NotImplementedError

    @property
    def examples(self) -> Optional[Dict[str, LazyProxy]]:
        return None

    @staticmethod
    def check(
            text: str,
            entities: ArgEntities
    ) -> bool:
        """
        Checks if the given text is valid for this type.
        It should check by the start of the text, as it could contain other arguments separated.
        """
        raise NotImplementedError

    def parse(
            self,
            text: str,
            offset: int,
            entities: ArgEntities
    ) -> Tuple[int, ArgValueType]:
        """Parses the arguments texts and returns the length of argument and value.
        Returns None if there's no more arguments."""

        raise NotImplementedError

    def pre_parse(self, *args, **_kwargs) -> Tuple[int, ArgValueType]:
        return self.parse(*args)

    @staticmethod
    def get_end(raw_text: str, entities: ArgEntities) -> Optional[int]:
        return None

    def __call__(
            self,
            text: str,
            offset: int,
            entities: ArgEntities,
            **kwargs) -> "ParsedArg[ArgValueType]":
        if not self.check(text, entities):
            raise ArgTypeError(
                needed_type=self.needed_type,
                description=self.description,
                offset=offset,
                length=self.get_end(text, entities) or len(text),
                examples=self.examples or {}
            )

        length, value = self.pre_parse(text, offset, entities, **kwargs)
        return ParsedArg(self, value, offset, length)

    def __repr__(self):
        return f"<{self.__class__.__name__}>"


ParsedArgType = TypeVar('ParsedArgType')


class ParsedArg(ABC, Generic[ParsedArgType]):
    """Argument object."""

    fabric: ArgFabric
    value: ParsedArgType

    offset: int
    length: int

    def __init__(self, fabric: ArgFabric, value: ParsedArgType, offset: int, length: int):
        self.fabric = fabric
        self.value = value
        self.offset = offset
        self.length = length

    @property
    def values(self) -> Tuple[ParsedArgType]:
        if not isinstance(self.value, list):
            raise TypeError("Value must be a ArgFabric")

        return tuple(x.value for x in self.value)

    def __repr__(self):
        return f"<{self.fabric}: {self.value=} {self.length=}>"


class OneWordArgFabricABC(ArgFabric, ABC):
    needed_type: Tuple[LazyProxy, LazyProxy]
    args_separator: str = ' '
    know_the_end = True

    def check_type(self, text: str) -> bool:
        raise NotImplementedError

    def check(self, text: str, entities: ArgEntities) -> bool:
        return text.lstrip() != ""

    def value(self, text: str) -> Any:
        raise NotImplementedError

    def parse(self, raw_text: str, offset: int, entities: ArgEntities) -> Tuple[int, Any]:
        first_word, *_rest = raw_text.split(self.args_separator, 1)

        if not self.check_type(first_word):
            raise ArgTypeError(
                # text=first_word,
                needed_type=self.needed_type,
                description=self.description,
                length=len(first_word),
                offset=offset,
                examples=self.examples or {}
            )

        return len(first_word), self.value(first_word)
