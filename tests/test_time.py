from datetime import timedelta

import pytest
from aiogram.utils.i18n import I18n

from ass_tg.exceptions import ArgTypeError
from ass_tg.i18n import gettext_ctx
from ass_tg.types import ActionTimeArg
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,result', [
    ('1y', timedelta(days=365)),
    ('1y2w', timedelta(days=365, weeks=2)),
    ('1y2w3d', timedelta(days=365 + 3, weeks=2)),
    ('1y2w3d4h', timedelta(days=365 + 3, weeks=2, hours=4)),
    ('1y2w3d4h5m', timedelta(days=365 + 3, weeks=2, hours=4, minutes=5))
])
def test_action_time(text: str, result: str):
    i18n = I18n(path='/')
    gettext_ctx.set(i18n)

    with i18n.context():
        arg = ActionTimeArg()(text, 5, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text', [
    '999',
    '2p',
    'WRONG_VALUE',
    '1y2p'
])
def test_action_time_fail(text: str):
    with pytest.raises(ArgTypeError):
        ActionTimeArg()(text, 5, NO_ENTITIES)
